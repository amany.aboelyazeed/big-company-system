# Big Company System

## Project Summary

BIG COMPANY project aims to assess the company's organizational's structure. 
It provides a way to do the following:
* Ensure each manager earns at least 20% more than their direct subordinates.
* Limit managers' earnings to no more than 50% above their direct reports' average salary.
* Identify employees with more than 4 layers of management between them and the CEO.


# Main Features
System will read company employee data from CSV file up to 1000 rows and identify:
- Which managers earn less than they should, and by how much.
- Which managers earn more than they should, and by how much.
- Which employees have a reporting line which is too long, and by how much.

## Input
CSV file with each line representing an employee (CEO included).  

- Data structure: Id,firstName,lastName,salary,managerId  
- CEO has no manager specified.  
- Number of rows can be up to 1000.  

Assumptions  
- The CSV employee input data has no cyclic dependencies, meaning that an employee defined as manager for subordinate won't have their subordinate ID assigned as their manager.
- Each employee has only one manager.
- Only one CEO is provided in csv file.

## Testing:
CompanyAssessmentControllerTest: run test providing csv file path to see company assessment results.


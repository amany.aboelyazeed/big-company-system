package com.bigcompany.calc;

import com.bigcompany.model.Employee;

import java.util.Map;

public interface AssessmentService {
    Map<Employee, Double> getUnderpaidManagers();

    Map<Employee, Double> getOverpaidManagers();

    Map<Employee, Integer> getEmployeesWithLongReportingChain();
}

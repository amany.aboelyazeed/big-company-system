package com.bigcompany.calc;

import com.bigcompany.model.Employee;
import com.bigcompany.repository.EmployeeRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import static com.bigcompany.config.AssessmentConstants.*;

public class CompanyAssessmentService implements AssessmentService {
    private final EmployeeRepository employeeRepository;

    public CompanyAssessmentService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    /**
     * @return Map of employee and underpaid salary amount that is less than the threshold.
     */
    @Override
    public Map<Employee, Double> getUnderpaidManagers() {
        Map<String, List<Employee>> managersWithEmployeesMap =
                this.employeeRepository.getAllManagersWithEmployees();

        return getManagersWithSalaryDiscrepancies(
                        managersWithEmployeesMap,
                        MIN_SALARY_PERCENTAGE,
                        (managerSalary, expectedSalary) -> managerSalary <= expectedSalary);
    }

    /**
     * @return Map of employee and overpaid salary amount that is exceeding threshold.
     */
    @Override
    public Map<Employee, Double> getOverpaidManagers() {
        Map<String, List<Employee>> managersWithEmployeesMap =
                this.employeeRepository.getAllManagersWithEmployees();

        return getManagersWithSalaryDiscrepancies(
                        managersWithEmployeesMap,
                        MAX_SALARY_PERCENTAGE,
                        (managerSalary, expectedSalary) -> managerSalary >= expectedSalary);
    }

    /**
     * @return Map of employee and Integer representing count of additional managers that are exceeding threshold.
     */
    @Override
    public Map<Employee, Integer> getEmployeesWithLongReportingChain() {
        Map<Employee, Integer> employeeManagerCountMap = new HashMap<>();
        for (Employee employee : this.employeeRepository.getAllEmployees()) {
            countEmployeeManagerChain(employee, employeeManagerCountMap);
        }

        return employeeManagerCountMap.entrySet().stream()
                .filter(employeeIntegerEntry -> employeeIntegerEntry.getValue() > MAX_EMPLOYEE_MANAGERS_COUNT)
                .collect(Collectors.toMap(Map.Entry::getKey,
                        value ->  value.getValue() - MAX_EMPLOYEE_MANAGERS_COUNT));
    }

    /**
     * Count number of all managers for each employee including CEO.
     * @param employee Employee object
     * @param employeeManagerCountMap map to keep track of employees that already has manager count calculated.
     * @return managers count
     */
    private int countEmployeeManagerChain(Employee employee, Map<Employee, Integer> employeeManagerCountMap) {
        if (employee == null || employee.isCEO()) {
            return 0;
        }

        if (employeeManagerCountMap.containsKey(employee)) {
            return employeeManagerCountMap.get(employee);
        } else {
            Employee employeeManager = this.employeeRepository.getById(employee.getManagerId());
            int managerCount = countEmployeeManagerChain(employeeManager, employeeManagerCountMap) + 1;
            employeeManagerCountMap.put(employee, managerCount);
            return managerCount;
        }
    }

    /**
     * Get which managers have salary different from expected range and by how much.
     * @param managersWithEmployeesMap Map of all manager ids with employee list of each manager
     * @param salaryPercentageThreshold Expected percentage of average subordinates salary
     * @param managerSalaryVerifier Predicate that tests manager salary comparing to expected salary
     * @return Map of managers which have salary discrepancies and by how much
     */
    private Map<Employee, Double> getManagersWithSalaryDiscrepancies(
            Map<String, List<Employee>> managersWithEmployeesMap,
            double salaryPercentageThreshold,
            BiPredicate<Double, Double> managerSalaryVerifier) {
        Map<Employee, Double> managersWithSalaryDiscrepancies = new HashMap<>();

        managersWithEmployeesMap.forEach((key, value) -> {
            double expectedSalary =
                    calculateManagerExpectedSalaryByPercentage(value,
                            salaryPercentageThreshold);
            Employee manager = this.employeeRepository.getById(key);

            if (managerSalaryVerifier.test(manager.getSalary(), expectedSalary)) {
                managersWithSalaryDiscrepancies.put(manager, Math.abs(expectedSalary - manager.getSalary()));
            }
        });

        return managersWithSalaryDiscrepancies;
    }

    /**
     * Calculate expected salary of a manager:
     * expected salary = average salary of direct subordinates * (1 + percentage /100)
     * @param subordinateList manager's direct subordinate list
     * @param expectedPercentage expected salary percentage
     * @return expected salary depending on percentage
     */
    private double calculateManagerExpectedSalaryByPercentage(List<Employee> subordinateList, double expectedPercentage) {
        double averageSubordinatesSalary = subordinateList.stream().mapToDouble(Employee::getSalary)
                .average().orElse(0);
        return averageSubordinatesSalary * (1 + expectedPercentage / 100);
    }

}

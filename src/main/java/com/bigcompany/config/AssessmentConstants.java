package com.bigcompany.config;

public final class AssessmentConstants {
    public static final int MAX_REPORTING_CHAIN_TO_CEO = 4;
    public static final int MAX_EMPLOYEE_MANAGERS_COUNT = MAX_REPORTING_CHAIN_TO_CEO + 1;
    public static final int MAX_SALARY_PERCENTAGE = 50;
    public static final int MIN_SALARY_PERCENTAGE = 20;

    private AssessmentConstants() {}


}

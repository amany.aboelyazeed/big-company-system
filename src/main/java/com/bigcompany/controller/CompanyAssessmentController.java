package com.bigcompany.controller;

import com.bigcompany.calc.AssessmentService;
import com.bigcompany.model.Employee;

import java.util.Map;

public class CompanyAssessmentController implements AssessmentController{
    private final AssessmentService assessmentService;

    public CompanyAssessmentController(AssessmentService assessmentService) {
        this.assessmentService = assessmentService;
    }

    /**
     * Print company assessment statistics:
     * - Which managers earn less than they should, and by how much.
     * - Which managers earn more than they should, and by how much.
     * - Which employees have a reporting line which is too long, and by how much.
     */
    @Override
    public void printCompanyAssessment() {
        printCompanyUnderpaidManagers();
        printCompanyOverpaidManagers();
        printEmployeesWithLongReportingChain();
    }

    private void printCompanyUnderpaidManagers() {
        Map<Employee, Double> underpaidManagers = assessmentService.getUnderpaidManagers();
        System.out.println("Company underpaid managers, count " + underpaidManagers.size());
        underpaidManagers.forEach((underpaidManager, salaryDiscrepancy) ->
                System.out.println(underpaidManager + " , with amount diff: " + salaryDiscrepancy));
    }

    private void printCompanyOverpaidManagers() {
        Map<Employee, Double> overpaidManagers = assessmentService.getOverpaidManagers();
        System.out.println("\nCompany overpaid managers, count " + overpaidManagers.size());
        overpaidManagers.forEach((overpaidManager, salaryDiscrepancy) ->
                System.out.println(overpaidManager + " , with amount diff: " + salaryDiscrepancy));
    }

    private void printEmployeesWithLongReportingChain() {
        Map<Employee, Integer> employeesWithLongReportingChain =
                assessmentService.getEmployeesWithLongReportingChain();
        System.out.println("\nCompany employees with long reporting chain between them and the CEO, count " + employeesWithLongReportingChain.size());
        employeesWithLongReportingChain.forEach((employee, numberOfManagers) ->
                System.out.println(employee + " , with additional: " + numberOfManagers + " managers"));
    }
}

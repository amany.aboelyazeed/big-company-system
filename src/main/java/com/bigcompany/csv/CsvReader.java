package com.bigcompany.csv;

import java.util.List;

public interface CsvReader<T> {
    List<T> readCsv();
}

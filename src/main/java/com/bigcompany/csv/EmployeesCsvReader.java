package com.bigcompany.csv;

import com.bigcompany.csv.exception.IncorrectFilePathException;
import com.bigcompany.csv.exception.InvalidEmployeeDataException;
import com.bigcompany.csv.exception.MissingEmployeeDataException;
import com.bigcompany.model.Employee;
import com.bigcompany.model.EmployeeBuilder;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class EmployeesCsvReader implements CsvReader<Employee> {
    private final static long MAX_LINES_COUNT = 1000;
    private String csvFileName;
    private boolean hasHeader;

    public EmployeesCsvReader(String csvFilePath, boolean hasHeader) {
        this.csvFileName = csvFilePath;
        this.hasHeader = hasHeader;
    }

    @Override
    public List<Employee> readCsv() {
        List<Employee> employeeList = new ArrayList<>();
        try (Stream<String> lines = java.nio.file.Files.lines(Paths.get(csvFileName))) {
            if (hasHeader) {
                lines.skip(1).limit(MAX_LINES_COUNT).forEach(line -> validateAndAddToEmployeeList(parseLine(line), employeeList));
            } else {
                lines.limit(MAX_LINES_COUNT).forEach(line -> validateAndAddToEmployeeList(parseLine(line), employeeList));
            }
        } catch (IOException e) {
            throw new IncorrectFilePathException("Filename is incorrect or file doesn't exist", e);
        }
        return employeeList;
    }

    private Employee parseLine(String line) {
        String[] data = line.split(",");
        if (data.length < 4) {
            throw new MissingEmployeeDataException("Line should have at least (id, firstname, lastname, salary), line:" + line);
        }
        EmployeeBuilder builder = new EmployeeBuilder()
                .id(data[0].trim())
                .firstName(data[1].trim())
                .lastName(data[2].trim())
                .salary(parseSalary(data[3]));
        if (data.length > 4) {
            builder.managerId(data[4].trim());
        }
        return builder.build();
    }

    private Double parseSalary(String stringSalary) {
        try {
            return Double.parseDouble(stringSalary.trim());
        } catch (NumberFormatException ex) {
            throw new InvalidEmployeeDataException("Invalid salary value: " + stringSalary);
        }
    }

    /**
     * Validate employee and add it to employee list.
     *
     * @param employeeToAdd new employee
     * @param employeeList  employee List
     */
    private void validateAndAddToEmployeeList(
            Employee employeeToAdd, List<Employee> employeeList) {
        if (isMandatoryValueMissing(employeeToAdd)) {
            throw new MissingEmployeeDataException("Mandatory employee data missing: " + employeeToAdd);
        }
        if (isDuplicateEmployeeId(employeeToAdd, employeeList)) {
            throw new InvalidEmployeeDataException("Employee duplicate Id:" + employeeToAdd.getId());
        }
        if (isInvalidCEO(employeeToAdd, employeeList)) {
            throw new InvalidEmployeeDataException("Only one CEO is expected, wrong data:" + employeeToAdd);
        }
        employeeList.add(employeeToAdd);
    }

    private boolean isMandatoryValueMissing(Employee employeeToAdd) {
        return employeeToAdd.getId().isEmpty()
                || employeeToAdd.getFirstName().isEmpty()
                || employeeToAdd.getLastName().isEmpty();
    }

    private boolean isDuplicateEmployeeId(Employee employeeToAdd, List<Employee> employeeList) {
        return employeeList.stream().anyMatch(employee -> employee.getId().equals(employeeToAdd.getId()));
    }

    private boolean isInvalidCEO(Employee employeeToAdd, List<Employee> employeeList) {
        return employeeToAdd.isCEO() && employeeList.stream().anyMatch(Employee::isCEO);
    }

}

package com.bigcompany.csv.exception;

public class IncorrectFilePathException extends RuntimeException {
    public IncorrectFilePathException(String errorMessage, Throwable ex) {
        super(errorMessage, ex);
    }
}

package com.bigcompany.csv.exception;

public class InvalidEmployeeDataException extends RuntimeException {
    public InvalidEmployeeDataException(String errorMessage) {
        super(errorMessage);
    }
}

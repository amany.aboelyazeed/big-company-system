package com.bigcompany.csv.exception;

public class MissingEmployeeDataException extends RuntimeException {
    public MissingEmployeeDataException(String errorMessage) {
        super(errorMessage);
    }
}

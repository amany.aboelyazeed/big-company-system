package com.bigcompany.model;

public class EmployeeBuilder {
    private String id;

    private String firstName;

    private String lastName;

    private double salary;

    private String managerId;

    public EmployeeBuilder id(String id) {
        this.id = id;
        return this;
    }

    public EmployeeBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public EmployeeBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public EmployeeBuilder salary(double salary) {
        this.salary = salary;
        return this;
    }

    public EmployeeBuilder managerId(String managerId) {
        this.managerId = managerId;
        return this;
    }

    public Employee build() {
        return new Employee(id, firstName, lastName, salary, managerId);
    }
}

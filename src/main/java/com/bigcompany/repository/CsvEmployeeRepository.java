package com.bigcompany.repository;

import com.bigcompany.csv.CsvReader;
import com.bigcompany.model.Employee;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class CsvEmployeeRepository implements EmployeeRepository {
    CsvReader<Employee> employeeCsvReader;

    private Map<String, Employee> employeeByIdMap;

    public CsvEmployeeRepository(CsvReader<Employee> csvReader) {
        this.employeeCsvReader = csvReader;
        loadEmployeeData();
    }

    private void loadEmployeeData() {
        List<Employee> employeeList = this.employeeCsvReader.readCsv();
        this.employeeByIdMap = employeeList.stream()
                .collect(Collectors.toMap(Employee::getId, Function.identity()));
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeByIdMap.values().stream().toList();
    }

    @Override
    public Employee getById(String employeeId) {
        return employeeByIdMap.get(employeeId);
    }

    @Override
    public Map<String, List<Employee>> getAllManagersWithEmployees() {
        return employeeByIdMap.values().stream()
                .filter(employee -> !employee.isCEO())
                .collect(groupingBy(Employee::getManagerId));
    }
}

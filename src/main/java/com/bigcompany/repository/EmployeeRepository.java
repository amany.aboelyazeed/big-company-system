package com.bigcompany.repository;

import com.bigcompany.model.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeRepository {

    /**
     * @return list of all employees
     */
    List<Employee> getAllEmployees();

    /**
     * Get employee object by id.
     * @param employeeId String Id
     * @return employee object
     */
    Employee getById(String employeeId);

    /**
     * @return Map of Manager key with list of all its subordinates.
     */
    Map<String, List<Employee>> getAllManagersWithEmployees();
}

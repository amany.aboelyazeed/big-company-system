package com.bigcompany.calc;

import com.bigcompany.csv.EmployeesCsvReader;
import com.bigcompany.model.Employee;
import com.bigcompany.model.EmployeeBuilder;
import com.bigcompany.repository.CsvEmployeeRepository;
import com.bigcompany.repository.EmployeeRepository;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class CompanyAssessmentServiceTest {
    private static final String FILE_PATH = "src/test/resources/";
    private AssessmentService assessmentService;

    private EmployeeRepository employeeRepository;


    @Test
    public void testGetEmployeesWithLongReportingChain() {
        employeeRepository = initializeEmployeeRepository("employeesWithLongManagerChain.csv");
        assessmentService = new CompanyAssessmentService(employeeRepository);

        Map<Employee, Integer> employeeMap = assessmentService.getEmployeesWithLongReportingChain();

        assertNotNull(employeeMap);
        assertEquals(2, employeeMap.size());
        assertEquals(getExpectedEmployeesWithLongManagerChain(), employeeMap);
    }

    @Test
    public void testGetOverpaidManagers() {
        employeeRepository = initializeEmployeeRepository("employeesWithOverpaidManagers.csv");
        assessmentService = new CompanyAssessmentService(employeeRepository);

        Map<Employee, Double> overpaidManagers = assessmentService.getOverpaidManagers();

        assertNotNull(overpaidManagers);
        assertEquals(3, overpaidManagers.size());
        assertEquals(getExpectedOverpaidManagers(), overpaidManagers);
    }

    @Test
    public void testGetUnderpaidManagers() {
        employeeRepository = initializeEmployeeRepository("employeesWithUnderpaidManagers.csv");
        assessmentService = new CompanyAssessmentService(employeeRepository);

        Map<Employee, Double> underpaidManagers = assessmentService.getUnderpaidManagers();

        assertNotNull(underpaidManagers);
        assertEquals(2, underpaidManagers.size());
        assertEquals(getExpectedUnderpaidManagers(), underpaidManagers);
    }

    private EmployeeRepository initializeEmployeeRepository(String fileName) {
        return new CsvEmployeeRepository(
                new EmployeesCsvReader(FILE_PATH + fileName, true));
    }

    private Map<Employee, Double> getExpectedOverpaidManagers() {
        return Map.of(
                new EmployeeBuilder().id("124").firstName("Martin").lastName("Chekov").salary(80000).managerId("123").build(), 5000d,
                new EmployeeBuilder().id("125").firstName("Bob").lastName("Ronstad").salary(70000).managerId("123").build(), 10000d,
                new EmployeeBuilder().id("127").firstName("Alex").lastName("Ronstad").salary(60000).managerId("123").build(), 15000d);
    }

    private Map<Employee, Double> getExpectedUnderpaidManagers() {
        return Map.of(
                new EmployeeBuilder().id("300").firstName("Alice").lastName("Hasacat").salary(50000).managerId("124").build(), 5600d,
                new EmployeeBuilder().id("125").firstName("Bob").lastName("Ronstad").salary(60000).managerId("123").build(), 1800d);
    }

    private Map<Employee, Integer> getExpectedEmployeesWithLongManagerChain() {
        return Map.of(
                new EmployeeBuilder().id("308").firstName("Brett").lastName("wwww").salary(34000).managerId("307").build(), 1,
                new EmployeeBuilder().id("309").firstName("Brett").lastName("qqqq").salary(34000).managerId("308").build(), 2);
    }
}
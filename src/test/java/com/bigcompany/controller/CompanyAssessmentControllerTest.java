package com.bigcompany.controller;

import com.bigcompany.calc.AssessmentService;
import com.bigcompany.calc.CompanyAssessmentService;
import com.bigcompany.csv.CsvReader;
import com.bigcompany.csv.EmployeesCsvReader;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.CsvEmployeeRepository;
import com.bigcompany.repository.EmployeeRepository;
import org.junit.Test;

public class CompanyAssessmentControllerTest {

    /**
     * Given below Company employee by Id hierarchy:
     *   123  --> 124 --> 300 --> 305
     *       |               --> 306
     *       |               --> 307 --> 308 --> 309 --> 310 --> 311
     *       |                                               --> 312
     *       |
     *       --> 125 --> 126
     *       |       --> 127
     *       |
     *       --> 127 --> 128
     *               --> 130
     */
    @Test
    public void testPrintCompanyAssessment() {
        CsvReader<Employee> employeeCsvReader = new EmployeesCsvReader(
                "src/test/resources/companyAssessment.csv", true);
        EmployeeRepository employeeRepository = new CsvEmployeeRepository(employeeCsvReader);
        AssessmentService assessmentService = new CompanyAssessmentService(employeeRepository);
        AssessmentController assessmentController = new CompanyAssessmentController(assessmentService);

        assessmentController.printCompanyAssessment();
    }
}
package com.bigcompany.csv;

import com.bigcompany.csv.exception.IncorrectFilePathException;
import com.bigcompany.csv.exception.InvalidEmployeeDataException;
import com.bigcompany.csv.exception.MissingEmployeeDataException;
import com.bigcompany.model.Employee;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EmployeesCsvReaderTest {
    private static final String FILE_PATH = "src/test/resources/";
    private CsvReader<Employee> employeeCsvReader;

    @Test
    public void testReadValidCsvFileWithHeader() {
        //Given
        employeeCsvReader = new EmployeesCsvReader(
                FILE_PATH + "employeesWithHeader.csv", true);

        //When
        List<Employee> employeeList = employeeCsvReader.readCsv();

        //Then
        assertNotNull(employeeList);
        assertEquals(5, employeeList.size());
    }

    @Test
    public void testReadValidCsvFileWithoutHeader() {
        employeeCsvReader = new EmployeesCsvReader(
                FILE_PATH + "employeesWithoutHeader.csv", false);

        List<Employee> employeeList = employeeCsvReader.readCsv();

        assertNotNull(employeeList);
        assertEquals(5, employeeList.size());
    }

    @Test(expected = IncorrectFilePathException.class)
    public void testIncorrectCsvFilePath() {
        employeeCsvReader = new EmployeesCsvReader("wrong.csv", false);

        employeeCsvReader.readCsv();
    }

    @Test(expected = MissingEmployeeDataException.class)
    public void testCsvFileWithIncompleteEmployeeData() {
        employeeCsvReader = new EmployeesCsvReader(
                FILE_PATH + "employeesWithMissingProperties.csv", true);

        employeeCsvReader.readCsv();
    }

    @Test(expected = InvalidEmployeeDataException.class)
    public void testCsvFileWithMultipleCEOs() {
        employeeCsvReader = new EmployeesCsvReader(
                FILE_PATH + "employeesWithMultipleCEOs.csv", true);

        employeeCsvReader.readCsv();
    }

    @Test(expected = InvalidEmployeeDataException.class)
    public void testCsvFileWithDuplicateEmployeeIds() {
        employeeCsvReader = new EmployeesCsvReader(
                FILE_PATH + "employeesWithDuplicateIds.csv", true);

        employeeCsvReader.readCsv();
    }

    @Test(expected = MissingEmployeeDataException.class)
    public void testCsvFileWithEmptyEmployeeId() {
        employeeCsvReader = new EmployeesCsvReader(
                FILE_PATH + "employeesWithMissingEmployeeId.csv", true);

        employeeCsvReader.readCsv();
    }

}
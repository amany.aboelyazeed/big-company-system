package com.bigcompany.repository;

import com.bigcompany.csv.CsvReader;
import com.bigcompany.model.Employee;
import com.bigcompany.model.EmployeeBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CsvEmployeeRepositoryTest {
    private EmployeeRepository employeeRepository;

    private CsvReader<Employee> csvReader;

    private List<Employee> mockEmployeeData;

    @Before
    public void setup() {
        csvReader = new MockCsvReader();
        employeeRepository = new CsvEmployeeRepository(csvReader);
        mockEmployeeData = getEmployeeMockData();
    }

    @Test
    public void testGetAllEmployees() {
        List<Employee> employees = employeeRepository.getAllEmployees();

        assertNotNull(employees);
        assertEquals(mockEmployeeData.size(), employees.size());
    }

    @Test
    public void testGetEmployeeById() {
        Employee expected =
                new EmployeeBuilder().id("124").firstName("name2").lastName("2").salary(800).managerId("123").build();

        Employee employee = employeeRepository.getById("124");

        assertNotNull(employee);
        assertEquals(expected, employee);
    }

    @Test
    public void testGetAllManagersWithEmployeesData() {
        Map<String, List<Employee>> result = employeeRepository.getAllManagersWithEmployees();

        assertNotNull(result);
        assertEquals(4, result.size());

        assertTrue(result.containsKey("123"));
        assertEquals(2, result.get("123").size());

        assertTrue(result.containsKey("124"));
        assertEquals(1, result.get("124").size());

        assertTrue(result.containsKey("300"));
        assertEquals(1, result.get("300").size());

        assertTrue(result.containsKey("305"));
        assertEquals(1, result.get("305").size());
    }

    private List<Employee> getEmployeeMockData() {
        return List.of(
                new EmployeeBuilder().id("123").firstName("name1").lastName("1").salary(900).build(),
                new EmployeeBuilder().id("124").firstName("name2").lastName("2").salary(800).managerId("123").build(),
                new EmployeeBuilder().id("125").firstName("name3").lastName("3").salary(700).managerId("123").build(),
                new EmployeeBuilder().id("300").firstName("name4").lastName("4").salary(600).managerId("124").build(),
                new EmployeeBuilder().id("305").firstName("name5").lastName("5").salary(500).managerId("300").build(),
                new EmployeeBuilder().id("306").firstName("name6").lastName("6").salary(500).managerId("305").build()
        );
    }

    private class MockCsvReader implements CsvReader<Employee> {

        @Override
        public List<Employee> readCsv() {
            return getEmployeeMockData();
        }
    }
}